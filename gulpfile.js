var gulp = require('gulp'),
    gutil = require('gulp-util'),
    typings = require('gulp-typings');
    


var build_folder = gutil.env.production ? 'production' : 'development';

gulp.task('default', function () 
{

});

gulp.task('ts',function()
{
  var tsProject = ts.createProject('./tsconfig.json');
  
  var tsResult = gulp.src('src/ts/**/*.ts')
                    .pipe(ts(tsProject));
            
  return tsResult.js.pipe(
          gulp.dest('builds/' + build_folder + '/assets/js'));
});

gulp.task('typings',function()
{
  return gulp.src("./typings.json")
          .pipe(typings()); 
});